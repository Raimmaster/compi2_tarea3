#ifndef _UTILS_H
#define _UTILS_H

#define SIZE_FLAGS 10
#include <string>
#include <sstream>
using namespace std;

class Gen
{
public:
    Gen()
    {
        this->code = "";
        this->place = -1;
        this->label = "";
    }
    string code;
    int place;
    string label;
};

#endif
