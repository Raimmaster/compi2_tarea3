#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include "ast.h"

using namespace std;

map<string, int> vars;
RegisterControl* reg_control = new RegisterControl();
LabelController label_control = LabelController();
int expt(int p, unsigned int q)
{
    int r = 1;

    while (q != 0) {
        if (q % 2 == 1) {    // q is odd
            r *= p;
            q--;
        }
        p *= p;
        q /= 2;
    }

    return r;
}

int InputExpr::evaluate()
{
	int result;

	cout << prompt;
	fflush(stdin);
	scanf("%d", &result);

	return result;
}

int CallExpr::evaluate()
{
    switch (fnId) {
        case FN_TIMECLOCK: return clock();
        case FN_RANDINT: {
            int start = arg0->evaluate();
            int end = arg1->evaluate();
            int range = end - start + 1;

            return (rand() % range) + start;
        }
        default:
            return 0;
    }
}

void BlockStatement::execute()
{
    list<Statement *>::iterator it = stList.begin();

    while (it != stList.end()) {
        Statement *st = *it;

        st->execute();
        it++;
    }
}

void PrintStatement::execute()
{
  list<Expr *>::iterator it = lexpr.begin();

  while (it != lexpr.end()) {
    Expr *expr = *it;

    if (expr->isA(STRING_EXPR)) {
      printf("%s", ((StringExpr*)expr)->str.c_str());
    } else {
      int result = expr->evaluate();
      printf("%d", result);
    }

    it++;
  }
  printf("\n");
}

void AssignStatement::execute()
{
    int result = expr->evaluate();
    vars[id] = result;
}

void IfStatement::execute()
{
    int result = cond->evaluate();

    if (result) {
        trueBlock->execute();
    } else if (falseBlock != 0) {
        falseBlock->execute();
    }
}

void WhileStatement::execute()
{
  int result = cond->evaluate();

  while (result) {
    block->execute();

    result = cond->evaluate();
  }
}

void ForStatement::execute()
{
	int val = startExpr->evaluate();
  	vars[id] = val;

	val = endExpr->evaluate();
	while (vars[id] < val) {
		block->execute();
		vars[id] = vars[id] + 1;
	}
}

void CallStatement::execute()
{
    switch (fnId) {
        case FN_RANDSEED: {
            int arg = arg0->evaluate();
            srand(arg);
        }
        default: {

        }
    }
}

//EXPR Generation

string NumExpr::generate()
{
    stringstream code;
    this->mips_code.place = reg_control->getNext();
    code<<"li $t"<<mips_code.place<<", "<<this->value<<endl;
    this->mips_code.code = code.str();
    return this->mips_code.code;
}

string IdExpr::generate()
{
    stringstream code;
    this->mips_code.place = reg_control->getNext();
    code<<"lw $t"<<this->mips_code.place<<", "<<this->id<<endl;
    this->mips_code.code = code.str();

    return this->mips_code.code;
}

string AddExpr::generate()
{
        string v1 = expr1->generate();
        string v2 = expr2->generate();
        reg_control->freePlace(expr1->mips_code.place);
        reg_control->freePlace(expr2->mips_code.place);
        stringstream code;
        code<<v1<<v2;
        this->mips_code.place = reg_control->getNext();
        code<<"add $t"<<this->mips_code.place<<", $t"<<expr1->mips_code.place<<", $t"<<expr2->mips_code.place<<endl;
        this->mips_code.code = code.str();
        return this->mips_code.code;
}

string SubExpr::generate()
{
        string v1 = expr1->generate();
        string v2 = expr2->generate();
        reg_control->freePlace(expr1->mips_code.place);
        reg_control->freePlace(expr2->mips_code.place);
        stringstream code;
        code<<v1<<v2;
        this->mips_code.place = reg_control->getNext();
        code<<"sub $t"<<this->mips_code.place<<", $t"<<expr1->mips_code.place<<", $t"<<expr2->mips_code.place<<endl;
        this->mips_code.code = code.str();
        return this->mips_code.code;
}

string MultExpr::generate()
{
    string v1 = expr1->generate();
    string v2 = expr2->generate();
    reg_control->freePlace(expr1->mips_code.place);
    reg_control->freePlace(expr2->mips_code.place);
    stringstream code;
    code<<v1<<v2;
    this->mips_code.place = reg_control->getNext();
    code<<"move $a0, $t"<<expr1->mips_code.place<<endl;
    code<<"move $a1, $t"<<expr2->mips_code.place<<endl;
    code<<"jal mult\nmove $t"<<mips_code.place<<", $v0"<<endl;
    this->mips_code.code = code.str();
    return this->mips_code.code;
}

string DivExpr::generate()
{
    string v1 = expr1->generate();
    string v2 = expr2->generate();
    reg_control->freePlace(expr1->mips_code.place);
    reg_control->freePlace(expr2->mips_code.place);
    stringstream code;
    code<<v1<<v2;
    this->mips_code.place = reg_control->getNext();
    code<<"move $a0, $t"<<expr1->mips_code.place<<endl;
    code<<"move $a1, $t"<<expr2->mips_code.place<<endl;
    //giving a direction to $a2 and $a3
    code<<"addi $sp, $sp, -8"<<endl;
    code<<"add $a2, $sp, $zero"<<endl;
    code<<"addi $a3, $sp, 4"<<endl;
    code<<"jal divide"<<endl;
    code<<"addi $sp, $sp, 8"<<endl;
    //obtaining the result
    code<<"lw $t"<<mips_code.place<<", 0($a2)"<<endl;

    this->mips_code.code = code.str();
    return this->mips_code.code;
}


string ModExpr::generate()
{
    string v1 = expr1->generate();
    string v2 = expr2->generate();
    reg_control->freePlace(expr1->mips_code.place);
    reg_control->freePlace(expr2->mips_code.place);
    stringstream code;
    code<<v1<<v2;
    this->mips_code.place = reg_control->getNext();
    code<<"move $a0, $t"<<expr1->mips_code.place<<endl;
    code<<"move $a1, $t"<<expr2->mips_code.place<<endl;
    //giving a direction to $a2 and $a3
    code<<"addi $sp, $sp, -8"<<endl;
    code<<"add $a2, $sp, $zero"<<endl;
    code<<"addi $a3, $sp, 4"<<endl;
    code<<"jal divide"<<endl;
    code<<"addi $sp, $sp, 8"<<endl;
    //obtaining the result
    code<<"lw $t"<<mips_code.place<<", 0($a3)"<<endl;

    this->mips_code.code = code.str();
    return this->mips_code.code;
}

string StringExpr::generate()
{
    // string label = label_control.newLabel("msg");
    // stringstream code;
    // code<<label<<": .asciz \""<<this->str<<"\""<<endl;
    // this->mips_code.label = label;
    // this->mips_code.code = code.str();
    string empty = "";
    return empty;
}

//Statement Generation

string PrintStatement::generate()
{
    stringstream code;
    for(auto it = lexpr.begin(); it != lexpr.end(); ++it)
    {
        Expr* expr = (*it);
        if(expr->getKind() == STRING_EXPR)
        {
            code<<"la $a0, "<<expr->mips_code.label<<endl;
            code<<"jal puts"<<endl;
        }else
        {
            code<<expr->generate();
            reg_control->freePlace(expr->mips_code.place);
            code<<"move $a0, $t"<<expr->mips_code.place<<endl;
            code<<"jal put_udecimal"<<endl;
        }
    }

    code<<"li $a0, '\\n'"<<endl;
    code<<"jal put_char"<<endl;
    code<<endl;


    return code.str();
}

string BlockStatement::generate()
{
    stringstream code;
    for(auto it = stList.begin(); it != stList.end(); ++it)
    {
        auto stmt = *it;
        code<<stmt->generate();
    }

    return code.str();
}

string AssignStatement::generate()
{
    stringstream code;
    code<<expr->generate();
    reg_control->freePlace(expr->mips_code.place);
    code<<"sw $t"<<expr->mips_code.place<<", ("<<id<<")"<<endl;

    return code.str();
}
