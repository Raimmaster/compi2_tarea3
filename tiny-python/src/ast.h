#ifndef _AST_H_
#define _AST_H_

#include <sstream>
#include <list>
#include <map>
#include <set>
#include "controllers.h"
#include "utils.h"

using namespace std;

extern map<string, int> vars;

int expt(int p, unsigned int q);

extern LabelController label_control;

enum BuiltInFunct {
    FN_TIMECLOCK,
    FN_RANDSEED,
    FN_RANDINT
};

enum ExprKind {
  LT_EXPR,
  LTE_EXPR,
  GT_EXPR,
  GTE_EXPR,
  NE_EXPR,
  EQ_EXPR,
  ADD_EXPR,
  SUB_EXPR,
  MULT_EXPR,
  DIV_EXPR,
  MOD_EXPR,
  EXPT_EXPR,
  NUM_EXPR,
  ID_EXPR,
  STRING_EXPR,
  INPUT_EXPR,
  CALL_EXPR
};

class Expr;
typedef list<Expr*> ExprList;

class Expr {
public:
    Gen mips_code;
    virtual int evaluate() = 0;
    virtual int getKind() = 0;
    virtual string generate() = 0;
    bool isA(int kind) { return (getKind() == kind); }
};

class BinaryExpr: public Expr {
public:
    BinaryExpr(Expr *expr1, Expr *expr2) {
        this->expr1 = expr1;
        this->expr2 = expr2;
    }

    Expr *expr1;
    Expr *expr2;
};

class LTExpr: public BinaryExpr {
public:
    LTExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() < expr2->evaluate(); }
    int getKind() { return LT_EXPR; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class GTExpr: public BinaryExpr {
public:
    GTExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() > expr2->evaluate(); }
    int getKind() { return GT_EXPR; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class LTEExpr: public BinaryExpr {
public:
    LTEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() <= expr2->evaluate(); }
    int getKind() { return LTE_EXPR; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class GTEExpr: public BinaryExpr {
public:
    GTEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() >= expr2->evaluate(); }
    int getKind() { return GTE_EXPR; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class NEExpr: public BinaryExpr {
public:
    NEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() != expr2->evaluate(); }
    int getKind() { return NE_EXPR; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class EQExpr: public BinaryExpr {
public:
    EQExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() == expr2->evaluate(); }
    int getKind() { return NE_EXPR; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class AddExpr: public BinaryExpr {
public:
    AddExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() + expr2->evaluate(); }
    int getKind() { return ADD_EXPR; }
    string generate();
};

class SubExpr: public BinaryExpr {
public:
    SubExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() - expr2->evaluate(); }
    int getKind() { return SUB_EXPR; }
    string generate();
};

class MultExpr: public BinaryExpr {
public:
    MultExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() * expr2->evaluate(); }
    int getKind() { return MULT_EXPR; }
    string generate();
};

class DivExpr: public BinaryExpr {
public:
    DivExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() / expr2->evaluate(); }
    int getKind() { return DIV_EXPR; }
    string generate();// { /*TODO*/ string e = ""; return e; }
};

class ModExpr: public BinaryExpr {
public:
    ModExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expr1->evaluate() % expr2->evaluate(); }
    int getKind() { return EXPT_EXPR; }
    string generate();// { /*TODO*/ string e = ""; return e; }
};

class ExponentExpr: public BinaryExpr {
public:
    ExponentExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    int evaluate() { return expt(expr1->evaluate(), expr2->evaluate()); }
    int getKind() { return MOD_EXPR; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class NumExpr: public Expr {
public:
    NumExpr(int value) { this->value = value; }
    int evaluate() { return value; }
    int getKind() { return NUM_EXPR; }
    string generate();
    int value;
};

class IdExpr: public Expr {
public:
    IdExpr(string id) { this->id = id; }
    int evaluate() { return vars[id]; }
    int getKind() { return ID_EXPR; }
    string generate();
    string id;
};

class StringExpr: public Expr {
public:
    StringExpr(string str) { this->str = str; }
    int evaluate() { return 0; }
    int getKind() { return STRING_EXPR; }
    string generate();

    string str;
};

class InputExpr: public Expr {
public:
    InputExpr(string prompt) { this->prompt = prompt; }
    int evaluate();
    int getKind() { return INPUT_EXPR; }

    string prompt;
    string generate() { /*TODO*/ string e = ""; return e; }
};

class CallExpr: public Expr {
public:
    CallExpr(BuiltInFunct fnId) {
        this->fnId = fnId;
    }
    CallExpr(BuiltInFunct fnId, Expr *arg0, Expr *arg1) {
        this->fnId = fnId;
        this->arg0 = arg0;
        this->arg1 = arg1;
    }
    int evaluate();
    int getKind() { return CALL_EXPR; }

    BuiltInFunct fnId;
    Expr *arg0, *arg1;
    string generate() { /*TODO*/ string e = ""; return e; }
};

enum StatementKind {
    BLOCK_STATEMENT,
    PRINT_STATEMENT,
    ASSIGN_STATEMENT,
    IF_STATEMENT,
    WHILE_STATEMENT,
	FOR_STATEMENT,
	PASS_STATEMENT,
    CALL_STATEMENT
};

class Statement {
public:
    virtual void execute() = 0;
    virtual StatementKind getKind() = 0;
    virtual string generate() = 0;
};

class BlockStatement: public Statement {
public:
    BlockStatement() {}
    void execute();
    StatementKind getKind() { return BLOCK_STATEMENT; }
	void add(Statement *st) { stList.push_back(st); }
    string generate();
    list<Statement *> stList;
};


class AssignStatement: public Statement {
public:
    AssignStatement(string id, Expr *expr) {
        this->id = id;
        this->expr = expr;
    }
    void execute();
    StatementKind getKind() { return ASSIGN_STATEMENT; }

    string id;
    Expr *expr;
    string generate();// { /*TODO*/ string e = ""; return e; }
};

class PrintStatement: public Statement {
public:
    PrintStatement(ExprList lexpr) {
        this->lexpr = lexpr;
    }
    void execute();
    StatementKind getKind() { return PRINT_STATEMENT; }
    string generate();

    ExprList lexpr;
};

class IfStatement: public Statement {
public:
    IfStatement(Expr *cond, Statement *trueBlock, Statement *falseBlock) {
        this->cond = cond;
        this->trueBlock = trueBlock;
        this->falseBlock = falseBlock;
    }
    void execute();
    StatementKind getKind() { return IF_STATEMENT; }

    Expr *cond;
    Statement *trueBlock;
    Statement *falseBlock;
    string generate() { /*TODO*/ string e = ""; return e; }
};

class PassStatement: public Statement {
public:
    PassStatement() {
    }
    void execute() {} ;
    StatementKind getKind() { return PASS_STATEMENT; }
    string generate() { /*TODO*/ string e = ""; return e; }
};

class WhileStatement: public Statement {
public:
    WhileStatement(Expr *cond, Statement *block) {
        this->cond = cond;
        this->block = block;
    }
    void execute();
    StatementKind getKind() { return WHILE_STATEMENT; }

    Expr *cond;
    Statement *block;
    string generate() { /*TODO*/ string e = ""; return e; }
};

class ForStatement: public Statement {
public:
    ForStatement(string id, Expr *startExpr, Expr *endExpr, Statement *block) {
        this->id = id;
        this->startExpr = startExpr;
        this->endExpr = endExpr;
        this->block = block;
    }
    void execute();
    StatementKind getKind() { return FOR_STATEMENT; }

    string id;
    Expr *startExpr;
    Expr *endExpr;
    Statement *block;
    string generate() { /*TODO*/ string e = ""; return e; }
};

class CallStatement: public Statement {
public:
    CallStatement(BuiltInFunct fnId, Expr *arg0) {
        this->fnId = fnId;
        this->arg0 = arg0;
        this->arg1 = NULL;
    }
    CallStatement(BuiltInFunct fnId, Expr *arg0, Expr *arg1): CallStatement(fnId, arg0) {
        this->arg1 = arg1;
    }
    void execute();
    StatementKind getKind() { return CALL_STATEMENT; }

    BuiltInFunct fnId;
    Expr *arg0, *arg1;
    string generate() { /*TODO*/ string e = ""; return e; }
};


class MipsGenerator
{
private:
    Statement* root_stmt;
    list<StringExpr*> str_list;
    // list<string> id_list;
    set<string> id_list;
public:
    MipsGenerator()
    {
        this->root_stmt = NULL;
    }

    void addString(StringExpr* str_node)
    {
        string label = label_control.newLabel("msg");
        stringstream code;
        code<<label<<": .asciz \""<<str_node->str<<"\""<<endl;
        str_node->mips_code.label = label;
        str_node->mips_code.code = code.str();
        this->str_list.push_back(str_node);
    }

    void addId(IdExpr* id_node)
    {
        stringstream code;
        code<<id_node->id<<": .word 0"<<endl;
        id_list.insert(code.str());
    }

    string generate()
    {
        stringstream code;
        code<<"#include \"screen.h\""<<endl;
        code<<"#include \"system.h\""<<endl;
        code<<".global main"<<endl;
        code<<".data"<<endl;
        for(auto it = str_list.begin();
            it != str_list.end(); ++it)
        {
            auto expr = *it;
            code<<expr->mips_code.code;
        }
        for(auto it = id_list.begin();
            it != id_list.end(); ++it)
        {
            auto str = *it;
            code<<str;
        }
        code<<".text"<<endl;
        code<<"main:"<<endl;
        code<<"li $a0, BRIGHT_WHITE"<<endl;
        code<<"li $a1, BLACK"<<endl;
        code<<"jal set_color"<<endl;
        code<<"jal clear_screen"<<endl;
        code<<root_stmt->generate();
        code<<"jr $ra"<<endl;
        return code.str();
    }

    void setRoot(Statement* stmt)
    {
        this->root_stmt = stmt;
    }
};

#endif
