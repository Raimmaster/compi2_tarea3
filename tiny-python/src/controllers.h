#ifndef _CONTROLLERS_H
#define _CONTROLLERS_H

#define SIZE_FLAGS 10

#include <string>
#include <sstream>
#include <iostream>

using namespace std;

class RegisterControl
{
private:
    bool flags[SIZE_FLAGS];
public:
    RegisterControl()
    {
        for (unsigned int i = 0; i < SIZE_FLAGS; i++)
        {
            flags[i] = false;
        }
    }
    int getNext();
    void freePlace(int);

};

class LabelController
{
private:
    unsigned int label_count;
    string label;
public:
    LabelController()
    {
        this->label_count = 0;
        this->label = "label";
    }

    string newLabel(string label_prefix)
    {
        stringstream st;
        st<<label_prefix<<label<<label_count++;
        return st.str();
    }
};
#endif
